# fail2ban

Additional files for fail2ban

## apache-log4shell

fail2ban filter to block hosts that try to exploit/test for CVE-2021-44228 
(log4shell). 

This is NOT a safeguard against log4shell, it's just reducing the lognoise.

See https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228 and
https://www.lunasec.io/docs/blog/log4j-zero-day/ for more
info on log4shell.



